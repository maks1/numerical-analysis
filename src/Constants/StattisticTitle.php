<?php

namespace App\Constants;

class StattisticTitle
{
    const START_INTERVAL = 'startInterval';
    const END_INTERVAL = 'endInterval';
    const START_VALUE = 'startValue';
    const DELTA = 'delta';
    const END_VALUE = 'endValue';

}