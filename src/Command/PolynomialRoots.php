<?php

namespace App\Command;

use App\Service\PolynomService\PolynomialCalculateInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;

abstract class PolynomialRoots extends Command
{
    /** @var PolynomialCalculateInterface */
    protected $polynomialCalculator;

    public function __construct(PolynomialCalculateInterface $polynomialCalculator)
    {
        $this->polynomialCalculator = $polynomialCalculator;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('polynomial', InputArgument::REQUIRED, 'polynomial coefficients');
        $this->addArgument('start', InputArgument::REQUIRED, 'start interval');
        $this->addArgument('end', InputArgument::REQUIRED, 'end interval');
        $this->addArgument('fault', InputArgument::REQUIRED, 'fault');
    }
}