<?php

namespace App\Command;

use App\Constants\StattisticTitle;
use App\Service\PolynomService\PolynomialSecantMethod;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PolynomialRootsBySecantMethodCommand extends PolynomialRoots
{
    protected static $defaultName = 'polynomial:secant:calculate';

    public function __construct(PolynomialSecantMethod $polynomialCalculator)
    {
        parent::__construct($polynomialCalculator);
    }

    protected function configure()
    {
        $this->setDescription('Calculating of polynomial roots by secant method');
        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $polynomial = $input->getArgument('polynomial');
        $this->polynomialCalculator->setPolynomial($polynomial);
        $fault = (float)$input->getArgument('fault');
        $this->polynomialCalculator->setEps($fault);
        $startInterval = (float)$input->getArgument('start');
        $endInterval = (float)$input->getArgument('end');
        $this->polynomialCalculator->calculate($startInterval, $endInterval);

        if ($this->polynomialCalculator->isSuccess()) {
            $statistic = $this->polynomialCalculator->getStatistic();

            $table = new Table($output);
            $table->setHeaders(['#', 'a', 'b', 'F(a)', 'F(b)']);

            foreach ($statistic as $num => $row) {
                $table->addRow([$num + 1, $row[StattisticTitle::START_INTERVAL], $row[StattisticTitle::END_INTERVAL], $row[StattisticTitle::START_VALUE], $row[StattisticTitle::END_VALUE]]);
            }
            $table->render();
            $output->writeln($this->polynomialCalculator->getResult());
        }
    }
}