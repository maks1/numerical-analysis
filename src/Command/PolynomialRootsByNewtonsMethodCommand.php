<?php

namespace App\Command;

use App\Constants\StattisticTitle;
use App\Service\PolynomService\PolynomialNewtonsMethod;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PolynomialRootsByNewtonsMethodCommand extends PolynomialRoots
{
    protected static $defaultName = 'polynomial:newton:calculate';

    public function __construct(PolynomialNewtonsMethod $polynomialCalculator)
    {
        parent::__construct($polynomialCalculator);
    }

    protected function configure()
    {
        $this->setDescription('Calculating of polynomial roots by Newton`s method');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $polynomial = $input->getArgument('polynomial');
        $this->polynomialCalculator->setPolynomial($polynomial);
        $fault = (float)$input->getArgument('fault');
        $this->polynomialCalculator->setEps($fault);
        $startInterval = (float)$input->getArgument('start');
        $endInterval = (float)$input->getArgument('end');
        $this->polynomialCalculator->calculate(($startInterval + $endInterval) / 2);

        if ($this->polynomialCalculator->isSuccess()) {
            $statistic = $this->polynomialCalculator->getStatistic();

            $table = new Table($output);
            $table->setHeaders(['#', 'a', 'F(a)', 'delta']);

            foreach ($statistic as $num => $row) {
                $table->addRow([$num + 1, $row[StattisticTitle::START_INTERVAL], $row[StattisticTitle::START_VALUE], $row[StattisticTitle::DELTA]]);
            }

            $table->render();
            $output->writeln($this->polynomialCalculator->getResult());
        }
    }
}