<?php

namespace App\Service\PolynomService;

use App\Constants\StattisticTitle;

class PolynomialBisectionMethod extends PolynomialBaseCalculate
{
    public function calculate(float $startPeriod, float $endPeriod = 0): void
    {
        if ($this->calculatePolynomial($startPeriod) * $this->calculatePolynomial($endPeriod) > 0) {
            $this->success = false;
            return;
        }
        $eps = $this->getEps();
        $middle = 0;
        while (($segment = $endPeriod - $startPeriod) > $eps) {
            $this->countIteration++;
            $startValue = $this->calculatePolynomial($startPeriod);
            $endValue = $this->calculatePolynomial($endPeriod);
            $this->statistic[] = [
                StattisticTitle::START_INTERVAL => $startPeriod,
                StattisticTitle::END_INTERVAL => $endPeriod,
                StattisticTitle::START_VALUE => $startValue,
                StattisticTitle::END_VALUE => $endValue
            ];
            $halfPeriod = $segment / 2;
            $middle = $startPeriod + $halfPeriod;

            if ($this->calculatePolynomial($startPeriod) * $this->calculatePolynomial($middle) < 0) {
                $endPeriod = $middle;
            } else {
                $startPeriod = $middle;
            }
        }

        $this->success = true;
        $this->result = $middle;
    }
}