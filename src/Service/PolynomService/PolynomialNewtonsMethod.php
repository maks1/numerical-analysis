<?php

namespace App\Service\PolynomService;

use App\Constants\StattisticTitle;

class PolynomialNewtonsMethod extends PolynomialBaseCalculate
{
    public function calculate(float $startPeriod, float $endPeriod = 0): void
    {
        $eps = $this->getEps();
        $h = 1e12;
        $x = $startPeriod;
        while (abs($h) >= $eps)
        {
            $h = $this->calculatePolynomial($x) / $this->calculateDerivPolynomial($x);
            $this->statistic[] = [
                StattisticTitle::START_INTERVAL => $x,
                StattisticTitle::START_VALUE => $this->calculatePolynomial($x),
                StattisticTitle::DELTA => $h,
            ];
            $x = $x - $h;
            $this->success = true;
        }
        $this->result = $x;
    }

    private function calculateDerivPolynomial($x)
    {
        $result = 0;
        foreach ($this->getPolynomials() as $key => $polynomial) {
            $result += ((int)$key * (int)$polynomial) * pow($x, ((int)$key - 1));
        }

        return $result;
    }
}