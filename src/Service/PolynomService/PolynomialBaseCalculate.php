<?php

namespace App\Service\PolynomService;

abstract class PolynomialBaseCalculate implements PolynomialCalculateInterface
{
    protected $result;
    protected $success = false;
    protected $countIteration = 0;
    protected $statistic = [];

    private $polynomials;
    private $eps;

    public function setPolynomial(string $polynomial): void
    {
        $coeff = explode(' ', $polynomial);
        if (count($coeff) === 0) {
            throw new \LogicException('Bad arguments');
        }
        $this->polynomials = array_reverse($coeff);
    }

    public function setEps(float $eps): void
    {
        $this->eps = $eps;
    }

    protected function calculatePolynomial(float $x): float
    {
        $result = 0;
        foreach ($this->polynomials as $key => $polynomial) {
            $result += ((int)$polynomial) * pow($x, (int)$key);
        }

        return $result;
    }

    protected function getEps(): float
    {
        return $this->eps;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function getResult(): float
    {
        return $this->result;
    }

    public function getStatistic(): array
    {
        return $this->statistic;
    }

    protected function getPolynomials(): array
    {
        return $this->polynomials;
    }
}