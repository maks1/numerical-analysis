<?php

namespace App\Service\PolynomService;

interface PolynomialCalculateInterface
{
    public function setPolynomial(string $polynomial): void;

    public function setEps(float $eps): void;

    public function calculate(float $startPeriod, float $endPeriod = 0): void;

    public function isSuccess(): bool;

    public function getResult(): float;

    public function getStatistic(): array;
}