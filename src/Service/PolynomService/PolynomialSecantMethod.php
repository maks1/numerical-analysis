<?php

namespace App\Service\PolynomService;

use App\Constants\StattisticTitle;

class PolynomialSecantMethod extends PolynomialBaseCalculate
{
    public function calculate(float $startPeriod, float $endPeriod = 0): void
    {
        if ($this->calculatePolynomial($startPeriod) * $this->calculatePolynomial($endPeriod) > 0) {
            $this->success = false;
            return;
        }
        $eps = $this->getEps();
        $intersection = 10e12;
        while (abs($this->calculatePolynomial($intersection)) > $eps) {
            $intersection = ($startPeriod * $this->calculatePolynomial($endPeriod) - $endPeriod * $this->calculatePolynomial($startPeriod)) / ($this->calculatePolynomial($endPeriod) - $this->calculatePolynomial($startPeriod));
            $this->countIteration++;
            $startValue = $this->calculatePolynomial($startPeriod);
            $endValue = $this->calculatePolynomial($endPeriod);
            $this->statistic[] = [
                StattisticTitle::START_INTERVAL => $startPeriod,
                StattisticTitle::END_INTERVAL => $endPeriod,
                StattisticTitle::START_VALUE => $startValue,
                StattisticTitle::END_VALUE => $endValue
            ];
            if ($this->calculatePolynomial($startPeriod) * $this->calculatePolynomial($intersection) < 0) {
                $endPeriod = $intersection;
            } else {
                $startPeriod = $intersection;
            }
        }

        $this->success = true;
        $this->result = $intersection;
    }
}